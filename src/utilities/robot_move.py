#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Bool, Float64
from geometry_msgs.msg import Twist, PoseStamped
from sensor_msgs.msg import LaserScan, JointState

class Robot:
    def __init__(self):
        # publishers and subscribers
        rospy.init_node("turtlebot3_controller")
        self.twist_publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
        self.left_wheel_vel_publisher = rospy.Publisher('/turtlebot3_burger/wheel_left_joint/set_speed', Float64, queue_size=10)
        self.right_wheel_vel_publisher = rospy.Publisher('/turtlebot3_burger/wheel_right_joint/set_speed', Float64, queue_size=10)
        self.scan = LaserScan()
        self.joint_states = JointState()
        self.rate = rospy.Rate(20)
        self.linear_speed = 0.0
        self.angular_speed = 0.0

        self.twist = Twist()
        self.vel = Float64()
        self.twist.linear.x = 0.0
        self.twist.linear.y = 0.0
        self.twist.linear.z = 0.0

        self.twist.angular.x = 0.0
        self.twist.angular.y = 0.0
        self.twist.angular.z = 0.0


        self.lidar_val = rospy.Subscriber('/scan', LaserScan, self.callback_get_lidar_values)
        self.j_states = rospy.Subscriber('/joint_states', JointState, self.callback_joint_states)
        

    def move(self,linear_speed):
        self.twist.linear.x = linear_speed
        self.twist_publisher.publish(self.twist)
        

    def rotate(self,angular_speed):
        self.twist.angular.z = angular_speed
        self.twist_publisher.publish(self.twist)

    def callback_joint_states(self,msg):
        #print("listen to Joint States")

        #print("selam: ", msg.position)

        self.joint_states.position = msg.position
        

    def callback_get_lidar_values(self,msg):
        #print("listen to lidar")
        
        current_time = rospy.Time.now()
        self.scan.header.stamp = current_time
        self.scan.header.frame_id = msg.header.frame_id
        self.scan.angle_min = msg.angle_min
        self.scan.angle_max = msg.angle_max
        self.scan.angle_increment = msg.angle_increment
        self.scan.time_increment = msg.time_increment
        self.scan.range_min = msg.range_min
        self.scan.range_max = msg.range_max
        self.scan.ranges = msg.ranges[0:72]
        self.scan.intensities = msg.intensities[0:72]
        
    def get_lidar_values(self):
        return self.scan
    def get_encoder_values(self):        
        return self.joint_states.position
    def is_ok(self):
        if not rospy.is_shutdown():
            self.rate.sleep()
            return True
        else:
            return False

    def set_right_wheel(self,vel):
        self.vel.data = vel * (1.0/0.10472) / 0.229
        self.right_wheel_vel_publisher.publish(self.vel)

    def set_left_wheel(self,vel):
        self.vel.data = vel * (1.0/0.10472) / 0.229
        self.left_wheel_vel_publisher.publish(self.vel)